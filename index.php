<?php 

require_once "Animal.php";
require_once "Frog.php";
require_once "Ape.php";

// Shaun
$sheep = new Animal("Shaun");
echo "Name: $sheep->name <br>";
echo "Legs: $sheep->legs <br>";
echo "Cold Blolded: $sheep->cold_blooded <br>";
echo "<br>";

// Frog
$frog = new Frog("Buduk");
echo "Name: $frog->name <br>";
echo "Legs: $frog->legs <br>";
echo "Cold Blolded: $frog->cold_blooded <br>";
echo "jump: " .$frog->jump() . "<br>";
echo "<br>";

// Ape
$ape = new Ape("Kera Sakti");
echo "Name: $ape->name <br>";
echo "Legs: $ape->legs <br>";
echo "Cold Blolded: $ape->cold_blooded <br>";
echo "Yell: " .$ape->yell() . "<br>";
echo "<br>";



